# Two polynomial algorithms for relabeling non-monotone data (work in progress)

A spring boot java project that implements two algorithms to make data sets monotone.

**first algorithm**: the greedy algorithm

**second algorithm**: the optimal flow network algorithm

TODO:
- Algorithm implementation